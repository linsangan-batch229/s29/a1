//Number 1 - Answer

db.courses.find({
    $and : [
        {instructor: 'Sir Rome'},
        {price: {$gte: 20000}}
    ] 
})

//Number 2 - Answer

db.courses.find(
    {instructor: "Ma'am Tine"},
    {_id: 0, name: 1}
)

//Number 3 - Answer

db.courses.find(
    {instructor: "Ma'am Miah"},
    {_id: 0, name: 1, price: 1}
)

//Number 4 - Answer

db.courses.find({
    $and : [
        {name : {$regex : 'r', $options : '$i'}},
        {price : {$gte : 20000}}
    ]
 })

//Number 5 - Answer

db.courses.find({
    $and : [
        {isActive : true},
        {price : {$lte : 25000}}
    ]
 })